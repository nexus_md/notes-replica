<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    const READ = 1;
    const UPDATE = 2;
    const DELETE = 3;

    const LVLS = [
        self::READ => "READ",
        self::UPDATE => "UPDATE",
        self::DELETE => "ADMIN",
    ];

    protected $table = 'notes';

    protected $fillable = [
        'title',
        'content',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'shared_notes', 'note_id', 'user_id')->withPivot('access_lvl');
    }

}
