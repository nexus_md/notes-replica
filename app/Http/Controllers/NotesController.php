<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotesRequest;
use App\Notes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Auth::user()->notes()->get();

        return view('user.notes.index')->withData($notes);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotesRequest $request)
    {
        Auth::user()->notes()->create($request->all());

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(Auth::user()->notes()->sharedNotes()-get());
        $notes = Auth::user()->sharedNotes()->find($id);

        if (empty($note))
            $notes = Auth::user()->notes()->findOrFail($id);

//        $notes = Auth::user()->notes()->findOrFail($id);
        $noteUsers = $notes->users()->get();
        $users = User::get();
        $access_lvls = Notes::LVLS;

        return view('user.notes.show')
                    ->withId($id)
                    ->withData($notes)
                    ->withUsers($users)
                    ->withNoteUsers($noteUsers)
                    ->withLvls($access_lvls);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->sharedNotes()->find($id);

        if (empty($note))
            Auth::user()->notes()->findOrFail($id);

        $note = Notes::where('id', $id)->get();

        return view('user.notes.edit')
            ->withFormData($note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Auth::user()->sharedNotes()->find($id);

        if (empty($note))
            $note = Auth::user()->notes()->findOrFail($id);

        $note->delete();
        return redirect()->back();
    }


    /**
     * Share the note Function
     *
     * @param $note_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function share($note_id, Request $request)
    {
        $user_id = $request->user_id;
        $access_lvl = $request->access_lvl;

        $note = Auth::user()->notes()->findOrFail($note_id);

        User::findOrFail($user_id);

        $note->users()->attach($user_id);

        $user = $note->users()->find($user_id);

        $user->pivot->access_lvl = $access_lvl;

        $user->pivot->save();

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayShared()
    {
        $notes = Auth::user()->sharedNotes()->get();

        dd(Auth::user()->sharedNotes()->get());

        return view('user.notes.shared')->withData($notes);
    }

    /**
     * Delete User From Shared
     *
     * @param $note_id
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function shareDelete($note_id, $user_id)
    {
        $note = Auth::user()->notes()->findOrFail($note_id);


        $note->users()->detach($user_id);
        return redirect()->back();
    }
}
