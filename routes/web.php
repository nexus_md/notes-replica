<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/notes', 'NotesController');
Route::get('/notes/{note_id}/share', 'NotesController@shareSelect')->name('notes.share');
Route::post('/notes/{note_id}/share', 'NotesController@share')->name('notes.share.add');
Route::get('/notes/{note_id}/share/{user_id}/delete', 'NotesController@shareDelete')->name('notes.share.delete');
Route::get('/notes/shared/view', 'NotesController@displayShared')->name('notes.shared.display');

Route::get('/home', 'HomeController@index')->name('home');