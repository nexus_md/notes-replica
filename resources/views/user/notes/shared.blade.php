@extends('layouts.app')

@section('content')
    <div id="archive-page" class="container">

        <form action="" method="GET" class="form-inline mb-2">
            <div class="form-group col-12">
                <input type="text" class="form-control mb-3" id="search" placeholder="Search for a log" name="search">
                <button type="submit" class="btn btn-primary mb-3">Search</button>
            </div>

        </form>

        <div id="notes" class="row">
            <table class="notes-wrap col-6 col-md-4">

                <thead class="card-header" id="heading">

                </thead>
                <tbody>
                @foreach($data as $value)

                    <tr class="mb-0">
                        <td>
                            <h5>
                                {{ $value->title }}
                            </h5>
                            <p>
                                {{ $value->content }}
                            </p>
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>

        </div>
    </div>
@stop


@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/archive.js') }}"></script>
@stop
