@extends('layouts.app')

@section('content')
    <div id="archive-page" class="container">

        <form action="" method="GET" class="form-inline mb-2">
            <div class="form-group col-12">
                <input type="text" class="form-control mb-3" id="search" placeholder="Search for a log" name="search">
                <button type="submit" class="btn btn-primary mb-3">Search</button>
            </div>

        </form>

        <div id="notes" class="row">
            @foreach($data as $value)
                <div class="notes-wrap col-6 col-md-4">

                    <div class="card-header" id="heading">
                        <h5 class="mb-0">
                            {{ $value->title }}
                        </h5>
                        <p>
                            {{ $value->content }}
                        </p>
                    </div>

                </div>
            @endforeach

        </div>
    </div>
@stop


@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/archive.js') }}"></script>
@stop
