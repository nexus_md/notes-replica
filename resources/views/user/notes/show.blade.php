@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-12">
                <h1> {{$data->title}} </h1>
            </div>

            <div class="col-12">
                <p>
                    {{$data->content}}
                </p>
            </div>

        </div>

        <div class="row">

            <form class="col-12" method="POST" action="{{ route('notes.share.add', $id) }}">

                {!! csrf_field() !!}

                <select name="user_id" class="col-12 col-md-3">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}"> {{ $user->name }} </option>
                    @endforeach
                </select>

                <select name="access_lvl" class="col-12 col-md-3">
                    @foreach($lvls as $key => $lvl)
                        <option value="{{ $key }}"> {{ $lvl }} </option>
                    @endforeach

                </select>
                <button class="col-12 col-md-3 btn" type="submit">
                    add
                </button>
            </form>
            <table class="col-12 table">
                <thead>
                    <td>User</td>
                    <td>Role</td>
                </thead>

                <tbody>
                    @foreach($noteUsers as $noteUser)
                        <tr>
                            <td>{{ $noteUser->name }}</td>

                            <td>{{ \App\Notes::LVLS[$noteUser->pivot->access_lvl] }}</td>

                            <td>
                                <a href="{{route('notes.share.delete', [$id ,$noteUser->id] )}}">x</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
@stop

@section('css')
@stop