@extends('layouts.app')

@section('content')
    <div class="container">

        <form id="create_note_form" action="{{ route('notes.share') }}" method="POST">
            {!! csrf_field() !!}

            {{-- Title --}}
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" class="form-control"/>
                <span class="help-block error-message"></span>
            </div>

            {{-- Content --}}
            <div class="form-group">
                <label for="content">Content</label>
                <textarea id="content" name="content" class="form-control"></textarea>
                <span class="help-block error-message"></span>
            </div>

            <button type="submit">
                Save
            </button>

        </form>
    </div>
@stop

@section('js')
@stop

@section('css')
@stop